package example.android.multiscreenapp

import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import example.android.multiscreenapp.R
import example.android.multiscreenapp.recyclerview.CommentAdapter
import example.android.multiscreenapp.recyclerview.ViewAdapter
import example.android.multiscreenapp.viewmodel.MainViewModel
import example.android.multiscreenapp.viewmodel.MainViewModelFactory

class CommentActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comments)

        val postId = intent.extras?.get("postId") as Int
        val viewModel = ViewModelProvider(this, MainViewModelFactory(this.application)).get(MainViewModel::class.java)
        val recyclerView = findViewById<RecyclerView>(R.id.commentRecyclerView)
        val adapter = CommentAdapter()

        //Listen to ViewModel to update the data on the RecycleView
        recyclerView.adapter = adapter
        viewModel.commentList.observe(this, {
            it?.let {
                var display = it.filter { e -> e.postId==postId }
                println(display)
                adapter.data = display
            }
        })
    }
}