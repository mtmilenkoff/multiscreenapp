package example.android.multiscreenapp.network

import example.android.multiscreenapp.database.Comment
import example.android.multiscreenapp.database.Post
import retrofit2.Call
import retrofit2.http.GET

interface ApiService{
    @GET("posts")
    fun getPosts() : Call<List<Post>>
    @GET("comments")
    fun getComments(): Call<List<Comment>>
}

