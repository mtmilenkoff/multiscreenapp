package example.android.multiscreenapp.recyclerview

import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import example.android.multiscreenapp.R

class PostViewHolder(view: View): RecyclerView.ViewHolder(view) {
    val title: TextView = view.findViewById(R.id.postTitle)
    val body: TextView = view.findViewById(R.id.body)
    val card: ConstraintLayout = view.findViewById(R.id.layout)
}
