package example.android.multiscreenapp.recyclerview

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import example.android.multiscreenapp.R
import example.android.multiscreenapp.CommentActivity
import example.android.multiscreenapp.database.Post

class ViewAdapter : RecyclerView.Adapter<PostViewHolder>() {

    var data = listOf<Post>()
    set(value) {
        field  = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.view_post, parent, false) as CardView
        return PostViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        val post = data[position]
        holder.body.text = post.body
        holder.title.text = post.title
        holder.card.setOnClickListener{
            val intent = Intent(it.context, CommentActivity::class.java)
            intent.putExtra("postId",post.id)
            it.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }
}