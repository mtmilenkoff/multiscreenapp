package example.android.multiscreenapp.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import example.android.multiscreenapp.R
import example.android.multiscreenapp.database.Comment

class CommentAdapter : RecyclerView.Adapter<CommentViewHolder>() {

    var data = listOf<Comment>()
        set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.view_comment, parent, false) as CardView
        return CommentViewHolder(view)
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        val comment = data[position]
        holder.email.text = comment.email
        holder.name.text = comment.name
        holder.body.text = comment.body
    }

    override fun getItemCount(): Int {
        return data.size
    }
}