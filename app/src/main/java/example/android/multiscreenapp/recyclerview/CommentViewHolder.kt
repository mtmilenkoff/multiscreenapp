package example.android.multiscreenapp.recyclerview

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import example.android.multiscreenapp.R

class CommentViewHolder(view: View): RecyclerView.ViewHolder(view) {
    val email: TextView = view.findViewById(R.id.email)
    val name: TextView = view.findViewById(R.id.name)
    val body: TextView = view.findViewById(R.id.body)
}