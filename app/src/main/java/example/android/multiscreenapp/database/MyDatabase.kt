package example.android.multiscreenapp.database

import android.content.Context
import androidx.room.*

@Database(entities = [Post::class, Comment::class], version = 1, exportSchema = false)
abstract class MyDatabase: RoomDatabase() {

    abstract val myDao: DatabaseDao
    companion object {
        @Volatile
        private var INSTANCE: MyDatabase? = null
        fun getInstance(context: Context): MyDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        MyDatabase::class.java,
                        "database"
                    ).fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}