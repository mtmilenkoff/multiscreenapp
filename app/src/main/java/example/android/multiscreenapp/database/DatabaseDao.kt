package example.android.multiscreenapp.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface DatabaseDao {
    @Insert (onConflict = OnConflictStrategy.REPLACE)
    fun insertAllPosts(posts: List<Post>)

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    fun insertAllComments(comments: List<Comment>)

    @Query ("SELECT * from posts_table")
    fun getAllPosts(): LiveData<List<Post>>

    @Query ("SELECT * from comments_table")
    fun getAllComments(): LiveData<List<Comment>>
}