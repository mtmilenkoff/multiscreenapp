package example.android.multiscreenapp.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "posts_table")
data class Post(val  userId: Int,
                @PrimaryKey
                var id: Int?,
                val title: String,
                val body: String)
