package example.android.multiscreenapp.database

import androidx.room.*

@Entity(tableName = "comments_table")
data class Comment(
    val postId: Int,
    @PrimaryKey
    val id: Int,
    val name: String,
    val email: String,
    val body: String)