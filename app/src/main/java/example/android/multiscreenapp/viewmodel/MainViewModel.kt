package example.android.multiscreenapp.viewmodel

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import example.android.multiscreenapp.MainActivity
import example.android.multiscreenapp.database.Comment
import example.android.multiscreenapp.repository.Repository
import example.android.multiscreenapp.database.MyDatabase
import example.android.multiscreenapp.database.Post
import kotlinx.coroutines.*

class MainViewModel(application: Application): AndroidViewModel(application) {

    private val database= MyDatabase.getInstance(application.applicationContext)
    private val repository = Repository(database)
    val postList = repository.postsList
    val commentList = repository.commentsList

    init {
        Log.i("MainViewModel","Created")
        updatePosts()
        updateComments()
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("ViewModel","Cleared")
    }

    fun updatePosts(){
        viewModelScope.launch {
            if(!repository.updatePosts()){
                Toast.makeText(getApplication<Application>().applicationContext,"Error: Couldn't update Posts",Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun updateComments(){
        viewModelScope.launch {
            if(!repository.updateComments()){
                Toast.makeText(getApplication<Application>().applicationContext,"Error: Couldn't update Comments",Toast.LENGTH_SHORT).show()
            }
        }
    }
}