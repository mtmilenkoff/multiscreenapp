package example.android.multiscreenapp.repository

import android.util.Log
import example.android.multiscreenapp.database.MyDatabase
import example.android.multiscreenapp.network.ApiConnection
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.await
import java.lang.Exception

class Repository(private val database: MyDatabase) {

    val postsList = database.myDao.getAllPosts()
    val commentsList = database.myDao.getAllComments()

    suspend fun updatePosts() : Boolean{
        val callSuccess: Boolean = withContext(Dispatchers.IO){
            val apiCall = ApiConnection.retrofitService.getPosts()
            try {
                val posts = apiCall.await()
                database.myDao.insertAllPosts(posts)
                return@withContext true
            }catch(e: Exception){
                Log.e("Error on api call: ", e.toString())
                return@withContext false
            }
        }
        return callSuccess
    }

    suspend fun updateComments() : Boolean{
        val callSuccess: Boolean = withContext(Dispatchers.IO){
            val apiCall = ApiConnection.retrofitService.getComments()
            try {
                val comments = apiCall.await()
                database.myDao.insertAllComments(comments)
                return@withContext true
            }catch(e: Exception){
                Log.e("Error on api call: ", e.toString())
                return@withContext false
            }
        }
        return callSuccess
    }
}