package example.android.multiscreenapp.repository

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import example.android.multiscreenapp.database.MyDatabase

class PostUpdateWorker(private val context: Context, params: WorkerParameters): CoroutineWorker(context, params) {

    companion object {const val WORK_NAME = "PostUpdateWorker"}

    override suspend fun doWork(): Result {
        val database = MyDatabase.getInstance(context)
        val repository = Repository(database)
        return if(repository.updatePosts()){
            Result.success()
        }else{
            Result.failure()
        }
    }
}