package example.android.multiscreenapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import example.android.multiscreenapp.database.MyDatabase
import example.android.multiscreenapp.databinding.ActivityMainBinding
import example.android.multiscreenapp.recyclerview.ViewAdapter
import example.android.multiscreenapp.repository.PostUpdateWorker
import example.android.multiscreenapp.viewmodel.MainViewModel
import example.android.multiscreenapp.viewmodel.MainViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val appScope = CoroutineScope(Dispatchers.Default)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewModel = ViewModelProvider(this, MainViewModelFactory(this.application)).get(MainViewModel::class.java)
        val adapter = ViewAdapter()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        //Listen to ViewModel to update the data on the RecycleView
        binding.postRecyclerView.adapter = adapter
        viewModel.postList.observe(this, {
            it?.let {
                adapter.data = it
            }
        })
        //Swipe refresh
        binding.swipeRefresh.setOnRefreshListener {
            binding.swipeRefresh.isRefreshing = true
            viewModel.updatePosts()
            binding.swipeRefresh.isRefreshing = false
            Toast.makeText(this, "Posts updated", Toast.LENGTH_SHORT).show()
        }
        //Start worker
        appScope.launch { setupWork() }
    }


    //Creates worker and sets it up on WorkManager
    private fun setupWork(){
        val updatePostsWork = PeriodicWorkRequestBuilder<PostUpdateWorker>(
            15,
            TimeUnit.MINUTES
        ).build()
        WorkManager.getInstance().enqueueUniquePeriodicWork(
            PostUpdateWorker.WORK_NAME,
            ExistingPeriodicWorkPolicy.KEEP,
            updatePostsWork)
    }
}